<#
GGSE PowerShell Profile
Created by Alexander Johnson (2012-2014)
Last update: 5/6/2014

This profile is meant to be autoloaded on the computer sending remote commands.
It has a few variables and functions defined. The working version needs to be
placed in C:\Windows\System32\WindowsPowerShell\v1.0\ with the name profile.ps1
Then it will load at the system level for all users.

You must run these from an ELEVATED PowerShell (right click, run as administrator).

Note : all the ` marks at the end of lines are for line continuation - I added
them to try and make the whole thing more readable.

#>

### Modules ###
# We need the AD module in order to get the computer list variables listed below

Import-Module ActiveDirectory

<### Variables ###
These variables are used to simplify where PowerShell commandlets are sent.

1. $all - this is every lab, classroom, and conference room computer
2. $edlab - this is the entire 4211 lab (including the lectern machines)
3. $4211 - this is only the 4211 iMacs (without the lectern machines)
4. $1211 - all machines in Ada's Lab
5. $lecterns - all the lectern mac minis in all classrooms (includes 4201)
6. $conference - all the conference room computers on the second / third floor

#>

$all = 
Get-AdComputer -Filter `
{(objectcategory -eq "computer")
 -AND (Name -like "4201-win*")
 -OR (Name -like "4211-win-1*")
 -OR (Name -like "4211-win-2*")
 -OR (Name -like "4211-win-3*")
 -OR (Name -like "4211-win-4*")
 -OR (Name -like "4211-win-5*")
 -OR (Name -like "4211-win-6*")
 -OR (Name -like "4211-win-7*")
 -OR (Name -like "4211-win-8*")
 -OR (Name -like "4211-win-9*")
 -OR (Name -like "4211-win-r*")
 -OR (Name -like "4211-win-b*")
 -OR (Name -like "*win-r") 
 -OR (Name -like "*win-b" )
 -OR (Name -like "12*-win*")
 -OR (Name -like "3*-win*" )
 -OR (Name -like "2*-win*" )} | ForEach-Object {$_.DNSHostname}

$edlab = 
Get-AdComputer -Filter `
{(objectcategory -eq "computer")
 -AND (Name -like "4211-win-*")
 -AND (Name -notlike "*-r" )
 -AND (Name -notlike "*-b")} | ForEach-Object {$_.DNSHostname}

$4211 = 
Get-AdComputer -Filter `
{(objectcategory -eq "computer")
 -AND (Name -like "4211-win-*") } | ForEach-Object {$_.DNSHostname}
 
$1211 = 
Get-AdComputer -Filter `
{(objectcategory -eq "computer")
 -AND (Name -like "1211-win-*") } | ForEach-Object {$_.DNSHostname}
 
$lecterns = Get-AdComputer -Filter `
{(objectcategory -eq "computer")
 -AND (Name -like "*win-r") 
 -OR (Name -like "*win-b" ) 
 -OR (Name -like "4201-win")} | ForEach-Object {$_.DNSHostname}
 
$conference = Get-AdComputer -Filter `
{(objectcategory -eq "computer")
 -AND (Name -like "2*-win") 
 -OR (Name -like "3*-win" )} | ForEach-Object {$_.DNSHostname}
 
### Functions ###

# checkUser function
function checkUsers ([string[]]$checkusercomputerlist){ ` 

<# This function hits computer to see if users are logged in. After that, it 
will respond by letting you know if there is a user logged in, if the computer
is available, or if the computer is unavailable (booted to OSX / offline).


    Example usage & output:

##########################################

PS H:\> checkUsers $all
4211-WIN-29  is available.
4211-WIN-28  is available.
4211-WIN-27  is available.

username       name
--------       ----
AD\bkiakeating 1201-WIN-R


1205-WIN-R  is available.
1203-WIN-R  is available.

username   name
--------   ----
AD\rberris 1211-WIN-6


1207-WIN-B  is available.
1205-WIN-B  is available.

username         name
--------         ----
AD\astanleyolson 1211-WIN-1


=====================================
Unavailable Computers:
=====================================
1211-WIN-3.ad.education.ucsb.edu
1211-WIN-5.ad.education.ucsb.edu
1211-WIN-4.ad.education.ucsb.edu
1211-WIN-2.ad.education.ucsb.edu
4211-WIN-19.ad.education.ucsb.edu
4211-WIN-17.ad.education.ucsb.edu

##########################################
This is the end of the documentation for the checkUsers function. 
Below is the actual function source.
#>

    # Create some empty arraylists                                                              
    $availablecomputers = New-Object System.Collections.ArrayList
    $unavailablecomputers = New-Object System.Collections.ArrayList

    #Check connectivity for each machine via Test-WSMan
    foreach ($computer in $checkusercomputerlist)
    {
        try 
        {
        Test-WSMan -ComputerName $computer -ErrorAction Stop |out-null
        $availablecomputers += $computer
        }
        catch 
        {
        $unavailablecomputers += $computer 
        }
    }
       
        # Retrieve the status & user via Get-WmiObject & format to a table
    Invoke-Command `
        -ComputerName  $availablecomputers `
        -ScriptBlock `
        {
            if
            ((Get-WmiObject win32_computersystem).username -like "AD\*")
            {(Get-WmiObject win32_computersystem) | Format-Table username, name -autosize}
            else 
            {
            Write-Host  $env:COMPUTERNAME " is available." -ForegroundColor Green
            }
        } `
        -EA Continue `
        
# Write to host which computers aren't available
if ($unavailablecomputers.Count -ne 0){
Write-Host "====================================="
Write-Host "Unavailable Computers:" -ForegroundColor Red
Write-Host "====================================="
$unavailablecomputers | ForEach {Write-Host "$_"}}
}

# restartComputers function
function restartComputers ([string[]]$restartcomputerlist){

<#

restartComputers take one argument after the function: a computer list or computer name
e.g. the variables for computer lists or specific host name (like 4211-win-b)


Note: if someone is logged in, this function will NOT restart the computer
(it will throw an error in red to tell you which computer). 

    Example usage & output:

##########################################

PS H:\> restartComputers $4211
=====================================
Restarted Computers:
=====================================
4211-WIN-27.ad.education.ucsb.edu
4211-WIN-29.ad.education.ucsb.edu
4211-WIN-28.ad.education.ucsb.edu
=====================================
Unavailable Computers:
=====================================
4211-WIN-IMAGE.ad.education.ucsb.edu
4211-WIN-19.ad.education.ucsb.edu
4211-WIN-17.ad.education.ucsb.edu
4211-WIN-14.ad.education.ucsb.edu
4211-WIN-30.ad.education.ucsb.edu
4211-WIN-7.ad.education.ucsb.edu

##########################################
This is the end of the documentation for restartComputers
Below is the actual source of the function.
#>


    # Create some empty arraylists                                                               
    $availablecomputers = New-Object System.Collections.ArrayList
    $unavailablecomputers = New-Object System.Collections.ArrayList

    #Check connectivity for each machine via Test-WSMan
    foreach ($computer in $restartcomputerlist)
    {
        try 
        {
        Test-WSMan -ComputerName $computer -ErrorAction Stop |out-null
        $availablecomputers += $computer
        }
        catch 
        {
        $unavailablecomputers += $computer 
        }
    }

Invoke-Command `
    -ComputerName $availablecomputers `
    -ScriptBlock {Restart-Computer}`
    -EA Continue `

# Write to host which computers were restarted
Write-Host "====================================="
Write-Host "Restarted Computers:" -ForegroundColor Green
Write-Host "====================================="
$availablecomputers | ForEach {Write-Host "$_"}

# Write to host which computers were unavailable
if ($unavailablecomputers.Count -ne 0){
Write-Host "====================================="
Write-Host "Unavailable Computers:" -ForegroundColor Red
Write-Host "====================================="
$unavailablecomputers | ForEach {Write-Host "$_"}}
}

function checkSoftware([string[]]$computers)
<#

The checksoftware function takes only one argument: a computer list

It will spit back a .CSV file that is saved to C:\temp of the machine your one.
The file will be named whatever the name of the computer is + "_software.csv".
The csv file includes the program names, versions, and install locations.
These lists are generated from reading a registry of uninstallable applications,
** so in very few / rare cirumstances, a program may be installed but not technically
"uninstallable" - so beware of this caveat.

    Example usage & output:

##########################################

PS H:\> checkSoftware helpdesk-win
Checking software on helpdesk-win
64-bit applications...
32-bit applications...
Check C:\temp for a csv file listing the installed software. The file name will include the DNS name of the computer.

##########################################
This is the end of the checkSoftware function documentation.
Below is the actual source to the function.
#>
    {
    foreach ($computer in $computers)
        {
        Write-Host "Checking software on $computer"
        
        $foo = 
        {
        # Go to uninstall registry for 64 bit applications and then sort & select the relevant properties,
        # that is, Name, Version, & Install location
        Write-Host "64-bit applications..." -ForegroundColor DarkYellow
            Get-ChildItem -Path HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall |
            Get-ItemProperty |
            Sort-Object -Property DisplayName |
            Select-Object -Property DisplayName, DisplayVersion, InstallLocation

        # Do the same thing but for 32 bit applications
        Write-Host "32-bit applications..." -ForegroundColor DarkYellow
        Get-ChildItem -Path HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall |
            Get-ItemProperty |
            Sort-Object -Property DisplayName |
            Select-Object -Property DisplayName, DisplayVersion, InstallLocation
            Select-Object -Property DisplayName, DisplayVersion, InstallLocation }

        Invoke-Command $computer -ScriptBlock $foo |
      
        # Finally, export the pipeline to a csv file located in C:\temp\ called $computer_software.csv
        Export-Csv -Path $("C:\temp\" + $computer + "_software.csv")  
        
        }
        Write-Host -ForegroundColor Green "Check C:\temp for a csv file listing the installed software. The file name will include the DNS name of the computer."
}