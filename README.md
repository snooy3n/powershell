PowerShell Profile
==================

Functions
---------
1. restartComputers
    + restarts computers as you'd expect
    + won't restart if someone is logged in
	+ returns a list of restarted & not restarted computers
	+ takes a single argument - a computer hostname or list of hostnames
    + Example: `restartComputers $1211`
2. checkUsers
	+ checks who is logged in
	+ returns if the computer is available or if someone is using it
	+ also returns a list of unreachable computers
	+ Example: `checkUsers $4211` <br>
3. checkSoftware
	+ scans the uninstall registries to see what is capable of being uninstalled
	+ selects relevant information and saves it to a .csv file
	+ saves the file in C:\temp
	+ Example: `checkSoftware @($1211, $4211)`

Installation Scripts
--------------------

1. SPSS - install_spss_22
	+ this is a crude attempt at automating SPSS installations
	+ it includes the authorization code
	+ it will run almost silently, but does have a few pop ups (terminals)
	+ it will need to be updated for the relevant version of SPSS
	+ it may need to be called locally e.g. the path to the ps1 file
2. Flash
	+ downloads msi's from a set location: flash for active x and flash plugin
	+ installs them silently
	+ can be called remotely
	+ Example: `Invoke-Command $1211 -FilePath {\\fs1\PublicTransfer\remote\installflash.ps1}`

Maintainence Scripts
--------------------

1. KeyServer duplicates
	+ sometimes one computer counts each OS as a seperate license
	+ we need to delete a few registries and a folder to create a clean slate
	+ I'm not sure if this works because we don't have any duplicates at the moment to test on
	+ BUT I know the method - (what you're done, not necessarily how its done in this script) - is correct. 