﻿<# 

Install MSI
1.15.2014 - Alexander Johnson

 #>

# Get the user tech name for Invoke-Command
$user_tech = Read-Host "User Tech"

# Specificy the target machines
$target = Read-Host "Target Machines (dns name or variable)"

# Specify the msi path and loop until the path is valid
DO {$msi = Read-host "Path to msi (must be a publicly accessible location e.g. Public Transfer)"}

WHILE((Test-Path $msi) -ne $true)

#Invoke the command & script block below
Invoke-Command `
    -ComputerName  $target `
    -Authentication Credssp `
    -Credential $user_tech `
    -ErrorAction Continue `
    -ArgumentList $msi `
    -ScriptBlock `
    {
    # Pass remote $msi varaible to local $msi variable
    param($msi)

    # this disables the warning as to whether or not you want to run an executable that isn't verified
    $env:SEE_MASK_NOZONECHECKS = 1

    # start the msi with /qn for silent install and log results in c:\temp
    Start-Process -FilePath $msi -ArgumentList '/qn /l* C:\msi_installation.log' -Wait

     #this enables to zone check again
    $env:SEE_MASK_NOZONECHECKS = 0
    }
    