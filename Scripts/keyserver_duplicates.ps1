﻿<#
 Delete KeyServer Preferences Script
 5/2014 - Alexander Johnson

 I'm not really sure if this works. We don't have a duplication problem
 at the moment but it has come up before in the past so I made this just
 in case someone else needs to try and fix it in the future, they'll have
 something to work off.

 Sometimes people don't know how to image computers with KeyServer
 involved. This often leads to extra information from the base 
 image (machine actually) being retained in the image and leads
 to KeyServer counting two operating systems of the same machine
 as two seperate computers due to one having the wrong identifier.

 The answer to this problem, for windows, is this:

1. Stop all processes of KeyServer, KeyAccess, or any other K2 process.

2. Remove the following registry entries.

HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\KeyAccess\Settings\pref
HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\KeyAccess\Settings\settings

3. Remove the following directory:
C:\ProgramData\KeyAccess\

4. Restart the machine and connect to KeyServer via KeyAcess. 
Check in KeyConfigure to see that the ComputerID is the same
across both operating systems.

Sassafras has great support and if you get lost, just call them.
#>

try{ 
Stop-Process -Name KeyAcc32 -Force
Write-Host "...KeyAcc32.exe"
}
catch{Write-Host $_.Exception.Message}

try{
Write-Host "Delete C:\ProgramFiles\KeyAcess\"
cd C:\ProgramData
rm -r .\KeyAcess
}
catch{Write-Hst $_.Exception.Message}

try{
# Get rid of directory
Write-Host "Deleting Registries:
HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\KeyAccess\Settings\pref
HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\KeyAccess\Settings\settings
"
cd hklm:\SYSTEM\CurrentControlSet\Services\KeyAccess\Settings
rm -r pref
rm -r settings
}

catch{
Write-Host $_.Exception.Message
}