﻿# Remote install for Flash 13
# Alexander Johnson 5/5/2014

# Specify the download source
$source = "http://download.macromedia.com/get/flashplayer/current/licensing/win/install_flash_player_13_active_x.msi"
# Specify  the download destination
$destination = "C:\flash.msi"
# Create a new webclient object
$wc = New-Object System.Net.WebClient
# Download the file
$wc.DownloadFile($source, $destination)
# Disable unverfified warning dialogue box
$env:SEE_MASK_NOZONECHECKS = 1
# Run MSI silently and log results to a text file - /i means install, /qn means silent & /L* means log everySthing
Start-Process -FilePath msiexec -ArgumentList /i, $destination, /qn -Wait

# Repeat for the plugin version
$source = "http://download.macromedia.com/get/flashplayer/current/licensing/win/install_flash_player_13_plugin.msi"
$destination = "C:\flashplugin.msi"
$wc = New-Object System.Net.WebClient
$wc.DownloadFile($source, $destination)
Start-Process -FilePath msiexec -ArgumentList /i, $destination, /qn -Wait
# Enable the dialogue box for unverified installations
$env:SEE_MASK_NOZONECHECKS = 0

# Remove the installers! a.k.a any trace of our existence. 
rm C:\flash.msi
rm C:\flashplugin.msi